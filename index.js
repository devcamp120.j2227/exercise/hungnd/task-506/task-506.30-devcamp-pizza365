// Import thư viện Express Js
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

app.use('/', (req, res, next) => {
    console.log(new Date());

    next();
});

// khai báo middleware
const {
    getAllDrinkMiddleware,
    getDrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
} = require('../task-506.30-devcamp-pizza365/app/routes/drinkRouter');

// tạo ra Router
const drinkRouter = express.Router();
// Khởi tạo các phương thức middleware
drinkRouter.get('/drinks', getAllDrinkMiddleware, (req, res) => {
    res.json({
        message: "Get all drink"
    })
});
// phương thức post
drinkRouter.post('/drinks', postDrinkMiddleware, (req, res) => {
    res.json({
        message: "Create a drink"
    })
});
// phương thức get drink by Id
drinkRouter.get('/drinks/:drinkId', getDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Get a drink by Id = ${drinkId}`
    })
});
// phương thức put drink by Id
drinkRouter.put('/drinks/:drinkId', putDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Update a drink by Id = ${drinkId}`
    })
});
// phương thức delete drink by Id
drinkRouter.delete('/drinks/:drinkId', deleteDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Delete a drink by Id = ${drinkId}`
    })
});
app.use('/', drinkRouter);
// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});